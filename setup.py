import os, sys
from setuptools import setup, find_packages

STATUS = ['Development Status :: 5 - Production/Stable']

def get_readme():
    try:
        return  open(os.path.join(os.path.dirname(__file__), 'README.rst')).read()
    except IOError:
        return ''

setup(
    name = 'eurocom-audit-log',
    version = '0.0.4',
    packages = find_packages(exclude = ['testproject']),
    author = 'Eurocom',
    author_email = 'sysadmin@eurocom.co.za',
    license = 'New BSD License (http://www.opensource.org/licenses/bsd-license.php)',
    description = 'Audit trail for django models',
    long_description = get_readme(),
    url = 'https://bitbucket.org/eurocom/eurocom-audit-log',
    include_package_data = True,
    zip_safe = False,
    
    classifiers = STATUS + [
       'Environment :: Plugins',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
)